<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CampagneVaccinationController extends AbstractController
{
    #[Route('/campagne_vaccination', name: 'app_campagne_vaccination')]
    public function index(): Response
    {
        return $this->render('campagne_vaccination/index.html.twig', [
            'controller_name' => 'CampagneVaccinationController',
        ]);
    }
}
