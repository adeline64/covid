<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SauverViesController extends AbstractController
{
    #[Route('/sauver_vies', name: 'app_sauver_vies')]
    public function index(): Response
    {
        return $this->render('sauver_vies/index.html.twig', [
            'controller_name' => 'SauverViesController',
        ]);
    }
}
