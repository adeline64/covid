<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CodePostalController extends AbstractController
{
    #[Route('/code_postal', name: 'app_code_postal')]
    public function index(): Response
    {
        return $this->render('code_postal/index.html.twig', [
            'controller_name' => 'CodePostalController',
        ]);
    }

}
